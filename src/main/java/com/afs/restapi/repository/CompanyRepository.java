package com.afs.restapi.repository;


import com.afs.restapi.entities.Company;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {
    private static final List<Company> companyList = new ArrayList<>();
    private static final AtomicLong atomicId = new AtomicLong(0);

    public Company insert(Company company) {
        company.setId(atomicId.incrementAndGet());
        companyList.add(company);
        return company;
    }

    public Company findById(Long id) {
        for (Company company : companyList) {
            if (Objects.equals(company.getId(), id)) {
                return company;
            }
        }
        return null;
    }

    public List<Company> findAll(){
        return companyList;
    }

    public List<Company> findByPage(Integer page, Integer size) {
        return companyList.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Company update(Long id, Company updateCompany) {
        for (Company company : companyList) {
            if (Objects.equals(id, company.getId())) {
                company.setName(updateCompany.getName());
                return company;
            }
        }
        return null;
    }

    public void deleteById(Long id) {
        companyList.removeIf(company -> Objects.equals(id, company.getId()));
    }

    public void clearAll(){
        companyList.clear();
    }
}
