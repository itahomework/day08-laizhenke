package com.afs.restapi.controller;

import com.afs.restapi.entities.Company;
import com.afs.restapi.entities.Employee;
import com.afs.restapi.service.CompanyService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    @Resource
    private CompanyService companyService;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Company addCompany(@RequestBody Company company) {
        return companyService.addCompany(company);
    }

    @GetMapping("/{id}")
    public Company findCompanyById(@PathVariable("id") Long id) {
        return companyService.findCompanyById(id);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> findCompanyListByPage(@RequestParam(value = "page", required = false) Integer page,
                                               @RequestParam(value = "size", required = false) Integer size) {
        return companyService.findCompanyList(page, size);
    }

    @GetMapping()
    public List<Company> findAllCompany() {
        return companyService.findAll();
    }

    @PutMapping("/{id}")
    public Company updateCompany(@PathVariable("id") Long id, @RequestBody Company company) {
        return companyService.updateCompany(id, company);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable("id") Long id) {
        companyService.deleteCompanyById(id);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> findEmployeeListByCompanyId(@PathVariable("id") Long id) {
        return companyService.findEmployeeListByCompanyId(id);
    }
}
