package com.afs.restapi.service;

import com.afs.restapi.entities.Company;
import com.afs.restapi.entities.Employee;

import java.util.List;

public interface CompanyService {
    Company addCompany(Company company);
    Company findCompanyById(Long id);
    List<Company> findCompanyList(Integer page, Integer size);
    void deleteCompanyById(Long id);
    List<Company> findAll();
    Company updateCompany(Long id, Company company);

    List<Employee> findEmployeeListByCompanyId(Long id);
}
