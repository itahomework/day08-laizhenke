package com.afs.restapi.service.impl;

import com.afs.restapi.entities.Company;
import com.afs.restapi.entities.Employee;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.CompanyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Resource
    private CompanyRepository companyRepository;
    @Resource
    private EmployeeRepository employeeRepository;

    @Override
    public Company addCompany(Company company) {
        return companyRepository.insert(company);
    }

    @Override
    public Company findCompanyById(Long id) {
        Company company = companyRepository.findById(id);
        if (company == null){
            throw new CompanyNotFoundException();
        }
        return companyRepository.findById(id);
    }

    @Override
    public List<Company> findCompanyList(Integer page, Integer size) {
        return companyRepository.findByPage(page, size);
    }

    @Override
    public void deleteCompanyById(Long id) {
        companyRepository.deleteById(id);
    }

    @Override
    public List<Company> findAll() {
        return companyRepository.findAll();
    }

    @Override
    public Company updateCompany(Long id, Company company) {
        return companyRepository.update(id, company);
    }

    @Override
    public List<Employee> findEmployeeListByCompanyId(Long id) {
        return employeeRepository.findEmployeesByCompanyId(id);
    }
}
