package com.afs.restapi.service;

import com.afs.restapi.entities.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> findAll();
    Employee findById(Long id);
    List<Employee> findByGender(String gender);
    List<Employee> findByPage(Integer page, Integer size);
    Employee create(Employee newEmployee);
    Employee update(Long id, Employee employee);
    void delete(Long id);
}
