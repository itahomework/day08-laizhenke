package com.afs.restapi.exception;

public class AgeIsInvalidException extends RuntimeException{
    public AgeIsInvalidException() {
        super("age is in valid");
    }
}
