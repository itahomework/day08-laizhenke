package com.afs.restapi.service.impl;

import com.afs.restapi.entities.Employee;

import com.afs.restapi.exception.AgeIsInvalidException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class EmployServiceImpl implements EmployeeService {

    @Resource
    private EmployeeRepository employeeRepository;

    public EmployServiceImpl (EmployeeRepository repository) {
        this.employeeRepository = repository;
    }


    @Override
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee findById(Long id) {
        Employee employee = employeeRepository.findById(id);
        if (employee == null){
            throw new EmployeeNotFoundException();
        }
        return employee;
    }

    @Override
    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    @Override
    public List<Employee> findByPage(Integer page, Integer size) {
        return employeeRepository.findByPage(page, size);
    }

    @Override
    public Employee create(Employee newEmployee) {
        if (newEmployee.ageIsInValid()){
            throw new AgeIsInvalidException();
        }
        if (newEmployee.ageAndSalaryIsInValid()){
            throw new RuntimeException("invalid");
        }
        newEmployee.setActive(true);
        return employeeRepository.insert(newEmployee);
    }

    @Override
    public Employee update(Long id, Employee employee) {
        Employee updateEmployee = findById(id);
        if (updateEmployee == null){
            throw new EmployeeNotFoundException();
        }
        if (!updateEmployee.isActive()){
            throw new EmployeeNotFoundException();
        }
        updateEmployee.setSalary(employee.getSalary());
        updateEmployee.setAge(employee.getAge());
        return employeeRepository.update(updateEmployee);
    }

    @Override
    public void delete(Long id) {
        Employee employee = findById(id);
        if (employee == null){
            throw new EmployeeNotFoundException();
        }
        employeeRepository.delete(employee);
    }
}
