package com.afs.restapi;

import com.afs.restapi.entities.Employee;
import com.afs.restapi.exception.AgeIsInvalidException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import com.afs.restapi.service.impl.EmployServiceImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class EmployeeServiceTest {

    private static final EmployeeRepository mokeEmployeeRepository = mock(EmployeeRepository.class);

    @Test
    void should_not_create_successfully_when_create_employee_given_age_is_invalid() {
        EmployeeService employeeService = new EmployServiceImpl(mokeEmployeeRepository);
        Employee employee = new Employee(1L, "sj", 15, "Female", 6000d, true, 1L);
        assertThrows(AgeIsInvalidException.class, () -> {
            employeeService.create(employee);
        });
    }

    @Test
    void should_not_create_successfully_when_create_employee_given_age_and_salary_is_invalid() {
        EmployeeService employeeService = new EmployServiceImpl(mokeEmployeeRepository);
        Employee employee = new Employee(1L, "sj", 3, "Female", 6000d, true, 1L);
        assertThrows(RuntimeException.class, () -> {
            employeeService.create(employee);
        });
    }

    @Test
    void should_not_create_successfully_when_create_employee_given_age_is_valid() {
        EmployeeService employeeService = new EmployServiceImpl(mokeEmployeeRepository);
        Employee employee = new Employee(1L, "sj", 19, "Female", 6000d, true, 1L);
        given(mokeEmployeeRepository.insert(any())).willReturn(employee);
        Employee createdEmployee = employeeService.create(employee);
        assertEquals(createdEmployee.getId(), employee.getId());
        assertEquals(createdEmployee.getName(), employee.getName());
    }


    @Test
    void should_set_in_active_when_delete_employee_given_an_active_employee() {
        EmployeeService employeeService = new EmployServiceImpl(mokeEmployeeRepository);
        Employee employee = new Employee(1L, "sj", 19, "Female", 6000d, true, 1L);
        employee.setActive(true);
        given(mokeEmployeeRepository.findById(1L)).willReturn(employee);

        employeeService.delete(1L);
        verify(mokeEmployeeRepository, times(1)).delete(employee);

    }

    @Test
    void should_throw_exception_when_update_given_active_is_false_employee(){
        EmployeeService employeeService = new EmployServiceImpl(mokeEmployeeRepository);
        Employee employee = new Employee(1L, "sj", 19, "Female", 6000d, true, 1L);
        employee.setActive(false);
        given(mokeEmployeeRepository.findById(1L)).willReturn(employee);

        assertThrows(EmployeeNotFoundException.class, () -> employeeService.update(1L, employee));
    }



}
