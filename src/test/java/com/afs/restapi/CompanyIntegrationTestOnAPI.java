package com.afs.restapi;

import com.afs.restapi.entities.Company;
import com.afs.restapi.entities.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.annotation.Resource;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyIntegrationTestOnAPI {
    @Resource
    private CompanyRepository companyRepository;
    @Resource
    private EmployeeRepository employeeRepository;
    @Resource
    private MockMvc client;

    @BeforeEach
    void clear() {
        companyRepository.clearAll();
    }

    @Test
    void should_return_created_company_perform_when_perform_insertCompany_given_company_json() throws Exception {
        // Given
        Company company = new Company(null, "baidu");
        String companyJson = new ObjectMapper().writeValueAsString(company);
        // When
        client.perform(MockMvcRequestBuilders.post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companyJson))
                // Then
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("baidu"));
    }

    @Test
    void should_return_companies_when_perform_findAllCompanies_given_companies() throws Exception {
        // Given
        Company company1 = new Company(null, "baidu");
        Company company2 = new Company(null, "huawei");
        companyRepository.insert(company1);
        companyRepository.insert(company2);
        // When
        client.perform(MockMvcRequestBuilders.get("/companies"))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(company1.getId()))
                .andExpect(jsonPath("$[0].name").value(company1.getName()))
                .andExpect(jsonPath("$[1].id").value(company2.getId()))
                .andExpect(jsonPath("$[1].name").value(company2.getName()))
        ;
    }

    @Test
    void should_return_employee_when_perform_findCompanyById_given_id() throws Exception {
        // Given
        Company company1 = new Company(null, "baidu");
        Company company2 = new Company(null, "huawei");
        companyRepository.insert(company1);
        companyRepository.insert(company2);
        // When
        client.perform(MockMvcRequestBuilders.get("/companies/{id}", company2.getId()))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(company2.getId()))
                .andExpect(jsonPath("$.name").value(company2.getName()))
        ;
    }

    @Test
    void should_return_companies_when_perform_findCompanyListByPage_given_page_size() throws Exception {
        // Given
        Company company1 = new Company(null, "baidu");
        Company company2 = new Company(null, "huawei");
        Company company3 = new Company(null, "baidu");
        Company company4 = new Company(null, "huawei");
        companyRepository.insert(company1);
        companyRepository.insert(company2);
        companyRepository.insert(company3);
        companyRepository.insert(company4);
        // When
        client.perform(MockMvcRequestBuilders.get("/companies")
                        .param("page", "1").param("size", "2"))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(company1.getId()))
                .andExpect(jsonPath("$[0].name").value(company1.getName()))
                .andExpect(jsonPath("$[1].id").value(company2.getId()))
                .andExpect(jsonPath("$[1].name").value(company2.getName()))
        ;
    }

    @Test
    void should_updated_company_when_perform_updateCompany_given_company_and_id() throws Exception {
        // Given
        Company company = new Company(null, "baidu");
        companyRepository.insert(company);
        Company updateCompany = new Company(null, "huawei");
        String updateEmployeeJson = new ObjectMapper().writeValueAsString(updateCompany);
        // When
        client.perform(MockMvcRequestBuilders.put("/companies/{id}", company.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updateEmployeeJson))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(updateCompany.getName()));
    }

    @Test
    void should_return_status_204_when_perform_deleteCompany_given_id() throws Exception {
        // Given
        Company company = new Company(null, "baidu");
        companyRepository.insert(company);
        // When
        client.perform(MockMvcRequestBuilders.delete("/companies/{id}", company.getId()))
                // Then
                .andExpect(status().isNoContent());
    }

    @Test
    void should_return_employees_when_perform_findEmployeeListByCompanyId_given_id() throws Exception {
        // Given
        Company company1 = new Company(null, "baidu");
        Company company2 = new Company(null, "huawei");
        companyRepository.insert(company1);
        companyRepository.insert(company2);
        Employee lucy = new Employee(null, "lucy", 21, "Female", 5000.0, true, company1.getId());
        Employee linda = new Employee(null, "linda", 21, "Female", 5000.0, true, company1.getId());
        Employee tom = new Employee(null, "tom", 21, "Male", 5000.0, true, company2.getId());
        employeeRepository.insert(lucy);
        employeeRepository.insert(linda);
        employeeRepository.insert(tom);
        // When
        client.perform(MockMvcRequestBuilders.get("/companies/{id}/employees", company1.getId()))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(lucy.getId()))
                .andExpect(jsonPath("$[0].name").value(lucy.getName()))
                .andExpect(jsonPath("$[1].id").value(linda.getId()))
                .andExpect(jsonPath("$[1].name").value(linda.getName()));
    }

}
